# -*- coding:utf-8 -*-

import json
import re

import requests

from define import SUBMIT_URL
from define import TIMEOUT
from util import check


def start():
    with open('proxy/proxy.txt', 'r') as proxy:
        for proxy_line in proxy:
            proxy = re.search('\d+\.\d+\.\d+\.\d+:\d+', proxy_line)
            if proxy:
                proxy_info = proxy_line.split(',')
                ip_port = proxy_info[0]
                ip = ip_port[0: ip_port.index(':')]
                port = ip_port[ip_port.index(':') + 1:]
                protocol = proxy_info[1].lower()
                location = proxy_info[3]
                speed = proxy_info[4]
                type = proxy_info[2]
                time = proxy_info[6].replace('\n', '')
                if check(ip, port, protocol):
                    proxy = {'ip': ip, 'port': port, 'protocol': protocol, 'location': location, 'type': type,
                             'time': time,
                             'speed': speed}
                    print('proxy:', proxy)
                    payload = {'proxy': json.dumps(proxy)}
                    requests.get(SUBMIT_URL, params=payload, timeout=TIMEOUT).text


if __name__ == '__main__':
    start()
