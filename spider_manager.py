# -*- coding:utf-8 -*-

import multiprocessing

import spider1
import spider2
import spider4


def start():
    multiprocessing.Process(target=spider1.start).start()
    multiprocessing.Process(target=spider2.start).start()
    multiprocessing.Process(target=spider4.start).start()


if __name__ == '__main__':
    start()
