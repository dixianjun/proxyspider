import requests

from define import TIMEOUT

__author__ = 'yueguang'

CHECK_URL = 'http://1212.ip138.com/ic.asp'
CHECK_HTTPS_URL = 'https://m.baidu.com/'

header = {
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.106 Safari/537.36'
}


def check(proxy_ip, proxy_port, protocol):
    if protocol == 'https':
        print(proxy_ip, proxy_port, protocol)
        return check_https(proxy_ip, proxy_port, protocol)
    elif protocol == 'http':
        return check_http(proxy_ip, proxy_port, protocol)
    else:
        return check_socks(proxy_ip, proxy_port, protocol)


def check_http(proxy_ip, proxy_port, protocol):
    try:
        protocol = str(protocol).lower()
        proxies = {protocol: protocol + '://' + proxy_ip + ':' + proxy_port}
        r = requests.get(CHECK_URL, headers=header, proxies=proxies, timeout=TIMEOUT)
        r.encoding = 'gbk'
        if r.status_code != 200:
            print('测试网站响应码错误:' + str(r.status_code))
        else:
            if str(proxy_ip) in r.text and '您的IP是' in r.text:
                print(protocol, proxy_ip, proxy_port, '有效')
                return True
    except Exception as e:
        print('测试网站请求出错:', e)
    print(protocol, proxy_ip, proxy_port, '无效')
    return False


def check_https(proxy_ip, proxy_port, protocol):
    try:
        protocol = str(protocol).lower()
        proxies = {protocol: protocol + '://' + proxy_ip + ':' + proxy_port}
        r = requests.get(CHECK_HTTPS_URL, headers=header, proxies=proxies, timeout=TIMEOUT)
        if r.status_code != 200:
            print('测试网站响应码错误:' + str(r.status_code))
        else:
            if '<title>百度一下</title>' in r.text:
                print(protocol, proxy_ip, proxy_port, '有效')
                return True
    except Exception as e:
        print('测试网站请求出错:', e)
    print(protocol, proxy_ip, proxy_port, '无效')
    return False


def check_socks(proxy_ip, proxy_port, protocol):
    # try:
    #     protocol = str(protocol).lower()
    #     proxies = {'http': protocol + '://' + proxy_ip + ':' + proxy_port,
    #                'https': protocol + '://' + proxy_ip + ':' + proxy_port}
    #     print(proxies)
    #     r = requests.get(CHECK_HTTPS_URL, headers=header, proxies=proxies,  timeout=TIMEOUT)
    #     if r.status_code != 200:
    #         print('测试网站响应码错误:' + str(r.status_code))
    #     else:
    #         if '<title>百度一下</title>' in r.text:
    #             print(protocol, proxy_ip, proxy_port, '有效')
    #             return True
    # except Exception as e:
    #     print('测试网站请求出错:', e)
    # print(protocol, proxy_ip, proxy_port, '无效')
    return False

if __name__ == '__main__':
    check('1.183.2.42', '8080', 'http')
