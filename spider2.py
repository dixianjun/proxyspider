import json
import multiprocessing
import time

import requests
from bs4 import BeautifulSoup

from define import MAX_PAGE_INDEX
from define import SUBMIT_URL
from define import TIMEOUT
from util import check

__author__ = 'yueguang'

CHINA_URL = 'http://www.xicidaili.com/nn/'
OTHER_URL = 'http://www.xicidaili.com/wn/'


def get_proxy_page(proxy_list_url):
    try:
        print('DEFAULT_URL + index:' + proxy_list_url)
        user_agent = {'User-agent': 'Mozilla/5.0'}
        response = requests.get(proxy_list_url, headers=user_agent, timeout=TIMEOUT)
        if response.status_code != 200:
            print('请求状态码错误:', response.status_code, response.text, response.content)
            return
        else:
            get_proxy(response.text)
    except Exception as e:
        print('请求代理列表出错:', e)


def get_proxy(content):
    soup = BeautifulSoup(content, 'lxml')
    proxy_table = soup.find_all('table')[0]
    all_proxy_info = proxy_table.find_all('tr')
    is_first = True
    for proxy_info in all_proxy_info:
        if is_first:
            is_first = False
            continue
        all_info = proxy_info.find_all('td')
        type = str(all_info[4].get_text())
        if type == '高匿':
            ip = all_info[1].get_text()
            port = all_info[2].get_text()
            location = str(all_info[3].get_text()).replace('\n', '').replace('\r', '').replace(' ', '')
            protocol = str(all_info[5].get_text()).lower()
            time = str(all_info[9].get_text())
            speed = str(float(all_info[6].div.get('title')[0: -1]) * 1000) + 'ms'
            if check(ip, port, protocol):
                proxy = {'ip': ip, 'port': port, 'protocol': protocol, 'location': location, 'type': type, 'time': time,
                         'speed': speed}
                payload = {'proxy': json.dumps(proxy)}
                requests.get(SUBMIT_URL, params=payload, timeout=TIMEOUT).text


def work(url):
    for i in range(1, MAX_PAGE_INDEX):
        proxy_list_url = url + str(i)
        get_proxy_page(proxy_list_url)


def start():
    multiprocessing.Process(target=work, args=(CHINA_URL,)).start()
    time.sleep(3)
    multiprocessing.Process(target=work, args=(OTHER_URL,)).start()


if __name__ == '__main__':
    start()
