# -*- coding:utf-8 -*-

import json

import tornado.ioloop
import tornado.web

import spider_manager

__author__ = 'yueguang'

proxy_list = []
safe_proxy_list = []
current_proxy_index = 0
current_safe_proxy_index = 0


class ProxyDistributeHandler(tornado.web.RequestHandler):
    def data_received(self, chunk):
        pass

    def get(self):
        global current_proxy_index
        if current_proxy_index + 1 < len(proxy_list):
            current_proxy_index += 1
        else:
            current_proxy_index = 0
        self.write(json.dumps(proxy_list[current_proxy_index]))
        print('下发代理:' + str(proxy_list[current_proxy_index]) + ' 在代理列表中下标:' + str(current_proxy_index))


class SafeProxyDistributeHandler(tornado.web.RequestHandler):
    def data_received(self, chunk):
        pass

    def get(self):
        global current_safe_proxy_index
        if current_safe_proxy_index + 1 < len(safe_proxy_list):
            current_safe_proxy_index += 1
        else:
            current_safe_proxy_index = 0
        self.write(json.dumps(safe_proxy_list[current_safe_proxy_index]))
        print('下发代理:' + str(safe_proxy_list[current_safe_proxy_index]) + ' 在代理列表中下标:' + str(current_safe_proxy_index))


class ProxySubmitHandler(tornado.web.RequestHandler):
    def data_received(self, chunk):
        pass

    def get(self):
        proxy = json.loads(self.get_argument('proxy'))
        if proxy['protocol'] == 'http':
            proxy_list.append(proxy)
        else:
            safe_proxy_list.append(proxy)
        print('新加代理:' + self.get_argument('proxy') + ' 当前代理数:' + str(len(proxy_list) + len(safe_proxy_list)))


class ProxyInfoHandler(tornado.web.RequestHandler):
    def data_received(self, chunk):
        pass

    def get(self):
        self.write('当前代理数 http:' + str(len(proxy_list)) + ' https:' + str(len(safe_proxy_list)))


def make_app():
    return tornado.web.Application([
        (r"/get_proxy", ProxyDistributeHandler),
        (r"/get_safe_proxy", SafeProxyDistributeHandler),
        (r"/submit_proxy", ProxySubmitHandler),
        (r"/get_proxy_info", ProxyInfoHandler)
    ])


if __name__ == '__main__':
    spider_manager.start()

    app = make_app()
    app.listen(8888)
    tornado.ioloop.IOLoop.current().start()
