# -*- coding: utf-8 -*-

import queue
import sqlite3
import threading
from datetime import datetime
from datetime import timedelta

dao = None


def run_in_db_thread(func):
    def _deco(*args):
        thread_id = threading.get_ident()
        if thread_id not in dao.result_queues:
            dao.result_queues[thread_id] = queue.Queue()
        id_method_args = (thread_id, func) + args
        dao.run_queue.put(id_method_args)
        ret = dao.result_queues[thread_id].get()
        return ret

    return _deco


class Dao(threading.Thread):
    def __init__(self):
        super().__init__()
        global dao
        dao = self
        self.db = None
        self.conn = None
        self.run_queue = queue.Queue()
        self.result_queues = {}

    def run(self):
        super().run()
        self.conn = sqlite3.connect('proxy.db')
        self.db = self.conn.cursor()
        self.db.execute(
            'CREATE TABLE IF NOT EXISTS proxy('
            'id INTEGER PRIMARY KEY AUTOINCREMENT,'
            'ip CHAR(15),'
            'port CHAR(5),'
            'protocol CHAR(10),'
            'last_check_time TIMESTAMP,'
            'is_useful BOOLEAN,'
            'first_get_time TIMESTAMP,'
            'source TEXT)')

        while True:
            id_method_args = self.run_queue.get()
            thread_id = id_method_args[0]
            method = id_method_args[1]
            args = id_method_args[2: len(id_method_args)]
            result = method(*args)
            dao.result_queues[thread_id].put(result)

    @run_in_db_thread
    def add_proxy(self, ip, port, protocol, source):
        """获取最近爬取到的没有监听且在线的主播"""
        d1 = datetime.now()
        d3 = d1 - timedelta(seconds=time_out)
        self.db.execute(
            'SELECT id FROM anchor WHERE is_live =? And (client = ? OR NOT client = ? AND last_alive_time < ?) '
            'ORDER BY last_get_time DESC LIMIT 1', (True, '', '', d3))
        info = self.db.fetchone()
        if info:
            return info[0]
        else:
            return None

    @run_in_db_thread
    def get_live_and_no_monitor_anchor_count(self, time_out):
        """获取没有监听且在线的主播数量"""
        d1 = datetime.now()
        d3 = d1 - timedelta(seconds=time_out)
        self.db.execute('SELECT count(id) FROM anchor WHERE is_live =? And (client = ? OR NOT client = ? AND '
                        'last_alive_time < ?)', (True, '', '', d3))
        info = self.db.fetchone()
        if info:
            return info[0]
        else:
            return 0

    @run_in_db_thread
    def commit(self):
        self.conn.commit()

    @run_in_db_thread
    def on_temp_spider_closed(self, closed_list):
        for closed_id in closed_list:
            print('清除数据库爬虫状态', closed_id)
            self.db.execute('UPDATE anchor SET client = ? WHERE client = ?', ('', closed_id))
        self.conn.commit()

    @run_in_db_thread
    def get_live_anchor_count_on_client(self, client, time_out):
        """获取指定客户端的监听的在线主播数"""
        d1 = datetime.now()
        d3 = d1 - timedelta(seconds=time_out)
        self.db.execute('SELECT COUNT(id) FROM anchor WHERE client = ? And is_live =? AND last_alive_time > ?',
                        (client, True, d3))
        info = self.db.fetchone()
        return info[0]

    @run_in_db_thread
    def get_live_anchor_count_on_clients(self, time_out):
        """获取所有客户端以及监听的在线主播数"""
        d1 = datetime.now()
        d3 = d1 - timedelta(seconds=time_out)
        self.db.execute(
            'SELECT client, COUNT(id) FROM anchor WHERE Not client = ? And is_live =? AND last_alive_time > ?'
            ' GROUP BY client', ('', True, d3))
        info = self.db.fetchall()
        return info

    @run_in_db_thread
    def on_anchor_monitor_live(self, anchor_id, client, is_live):
        """主播在监听"""
        if is_live:
            self.db.execute('UPDATE anchor SET client = ?, last_alive_time = ?, is_live =? WHERE id = ?',
                            (client, datetime.now(), is_live, anchor_id))
        else:
            self.db.execute('UPDATE anchor SET client = ?, is_live =? WHERE id = ?', ('', is_live, anchor_id))

    @run_in_db_thread
    def on_get_anchor(self, anchor_id, list_spider_time_range):
        """主播被爬取到"""
        self.db.execute('SELECT all_show_time FROM anchor WHERE id =?', (anchor_id,))
        time_now = datetime.now()
        info = self.db.fetchone()
        if info:
            all_show_time = info[0]
            self.db.execute('UPDATE anchor SET last_get_time = ?, all_show_time = ?, is_live =? WHERE id = ?',
                            (time_now, all_show_time + list_spider_time_range, True, anchor_id))
        else:
            self.db.execute('INSERT INTO anchor VALUES (?,?,?,?,?,?,?)',
                            (anchor_id, time_now, time_now, list_spider_time_range, -1, '', True))


if __name__ == '__main__':
    bb = ["vick-ThinkPad-T430:vick"]
    db = Dao()
    db.start()
    aa = db.on_temp_spider_closed(bb)
    print(aa)
