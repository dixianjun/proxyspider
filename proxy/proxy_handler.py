# -*- coding:utf-8 -*-
import re

import threadpool

from util import check

proxy_list = {}
proxy_list_all = []
proxy_list_useful = []


def pre_handle():
    with open('proxy/proxy.txt', 'r') as proxy:
        for proxy_info in proxy:
            proxy = re.search('\d+\.\d+\.\d+\.\d+:\d+', proxy_info)
            if proxy:
                if proxy.group() not in proxy_list:
                    proxy_list[proxy.group()] = proxy_info
                    proxy_list_all.append(proxy_info)
                else:
                    print(proxy.group(), '重复')
    print(len(proxy_list))


def check_proxy(proxy_line):
    proxy_info = proxy_line.split(',')
    ip_port = proxy_info[0]
    ip = ip_port[0: ip_port.index(':')]
    port = ip_port[ip_port.index(':') + 1:]
    protocol = proxy_info[1].lower()
    if check(ip, port, protocol):
        proxy_list_useful.append(proxy_line)


if __name__ == '__main__':
    pre_handle()
    pool = threadpool.ThreadPool(10)
    requests = threadpool.makeRequests(check_proxy, proxy_list_all)
    for req in requests:
        pool.putRequest(req)
    pool.wait()
    with open('proxy/proxy_handle.txt', 'w') as proxy_new:
        for proxy in proxy_list_useful:
            print(proxy)
            proxy_new.write(proxy)
