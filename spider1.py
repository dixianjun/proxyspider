# -*- coding:utf-8 -*-

import json
import multiprocessing
import time

import requests
from bs4 import BeautifulSoup

from define import MAX_PAGE_INDEX
from define import SUBMIT_URL
from define import TIMEOUT
from util import check

CHINA_URL = 'http://www.kuaidaili.com/free/inha/'
OTHER_URL = 'http://www.kuaidaili.com/free/outha/'


def get_proxy_page(proxy_list_url):
    try:
        print('DEFAULT_URL + index:' + proxy_list_url)
        time.sleep(3)
        response = requests.get(proxy_list_url, timeout=TIMEOUT)
        if response.status_code != 200:
            print('请求状态码错误:', response.status_code)
            return
        else:
            get_proxy(response.text)
    except ConnectionError as e:
        print('请求代理列表出错:', e)


def get_proxy(content):
    soup = BeautifulSoup(content, 'lxml')
    all_tables = soup.find_all('table')
    proxy_table = None
    for table in all_tables:
        if 'table-bordered' in table['class']:
            proxy_table = table
            break
    if proxy_table:
        all_proxy_info = proxy_table.find_all('tr')
        is_first = True
        for proxy_info in all_proxy_info:
            if is_first:
                is_first = False
                continue
            all_info = proxy_info.find_all('td')
            type = str(all_info[2].get_text())
            if type == '高匿名':
                ip = str(all_info[0].get_text()).replace('\n', '').replace('\t', '').replace('\r', '')
                port = str(all_info[1].get_text()).replace('\n', '').replace('\t', '').replace('\r', '')
                location = str(all_info[4].get_text())
                protocol = str(all_info[3].get_text()).lower()
                if len(protocol) > 4:
                    protocol = 'https'
                time = str(all_info[6].get_text())
                speed = str(all_info[5].get_text())
                if check(ip, port, protocol):
                    proxy = {'ip': ip, 'port': port, 'protocol': protocol, 'location': location, 'type': type, 'time': time,
                             'speed': speed}
                    payload = {'proxy': json.dumps(proxy)}
                    requests.get(SUBMIT_URL, params=payload, timeout=TIMEOUT).text
    else:
        print('err:代理列表解析失败')
        print(content)


def work(url):
    for i in range(1, MAX_PAGE_INDEX):
        proxy_list_url = url + str(i)
        get_proxy_page(proxy_list_url)


def start():
    multiprocessing.Process(target=work, args=(CHINA_URL, )).start()
    time.sleep(3)
    multiprocessing.Process(target=work, args=(OTHER_URL, )).start()


if __name__ == '__main__':
    start()
